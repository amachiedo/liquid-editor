﻿# Liquid Editor

## Install requirements

> yarn install

## To start a server with hot-reload

> yarn start

## To build the app

> yarn build
