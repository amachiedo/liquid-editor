// @flow

import * as React from 'react'
import Liquid from 'liquidjs'
import * as _ from 'lodash'
import { Parser as HtmlToReactParser } from 'html-to-react'
import './App.css'

type Props = {}

type State = {
  parsedNode: ?React.Node,
  error: ?string,
  params: any,
  template: Array<any>
}

class App extends React.Component<Props, State> {
  _htmlToReactParser: HtmlToReactParser
  _liquidEngine: Liquid
  handleOnChange: SyntheticInputEvent<HTMLInputElement> => void
  handleOnInput: SyntheticInputEvent<HTMLTextAreaElement> => void

  constructor(props: Props) {
    super(props)

    this._htmlToReactParser = new HtmlToReactParser()
    this._liquidEngine = new Liquid({
      strict_filters: true,
      strict_variables: true
    })
    this.handleOnChange = this.handleOnChange.bind(this)
    this.handleOnInput = this.handleOnInput.bind(this)

    this.state = {
      parsedNode: null,
      error: null,
      params: {},
      template: []
    }
  }

  handleOnChange(event: SyntheticInputEvent<HTMLInputElement>) {
    const { id, value } = event.target
    const { params, template } = this.state
    let newParams = {...params}

    if (!value) {
      const parentProp = id.split('.')[0]
      delete newParams[parentProp]
    }
    else {
      newParams = _.set(params, id, value)
    }

    this.renderTemplate(template, newParams)
    this.setState({ params: newParams })
  }

  handleOnInput(event: SyntheticInputEvent<HTMLTextAreaElement>) {
    const { value } = event.target
    const { params, template } = this.state
    let newParams = {...params}
    
    try {
      const newTemplate = this._liquidEngine.parse(value.replace(/(\${)(\w+)(})/g, '$2'))

      // if there is a token which existed in the old template
      // but it's not in new template, remove it from params
      const newTokenIds = newTemplate.filter(token => token.type === 'value').map(token => token.initial)
      const oldTokenIds = template.filter(token => token.type === 'value').map(token => token.initial)
      const tokenIdsToRemove = oldTokenIds.filter(newTokenId => !newTokenIds.includes(newTokenId))

      for (const tokenId of tokenIdsToRemove) {
        delete newParams[tokenId]
      }

      this.renderTemplate(newTemplate, newParams)
      this.setState({
        params: newParams,
        template: newTemplate
      })
    } catch (error) {
      this.setState({error: error.message })
    }
  }

  renderTemplate(template: Array<any>, params: any) {
    this._liquidEngine
      .render(template, params)
      .then(result =>
        this.setState({ 
          parsedNode: this._htmlToReactParser.parse(result),
          error: null 
        })
      )
      .catch(error => this.setState({error: error.message }))
  }

  render() {
    const {
      parsedNode,
      error,
      template
    } = this.state

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Liquid Template Editor</h1>
        </header>
        <div className="editor">
          <div className="tab">
            Editor
          </div>
          <textarea onInput={this.handleOnInput} />
        </div>
        <div className="editor">
          <div className="tab">
            Preview 
          </div>
          <div className="preview">
            {
              error ? (
                <div style={{ color: 'red' }}>
                  {error}
                </div>
              ) : parsedNode
            }
          </div>
        </div>
        <div
          className="inputs"
          style={{
            margin: '20px',
            textAlign: 'left'
          }}>
          { 
            template ? (
              _.uniq(template.filter(token => token.type === 'value')
              .map(token => token.initial))
              .map(initial => {
                return (
                  <div
                    key={initial}
                    style={{
                      padding: '5px'
                    }}>
                    <label
                      htmlFor={initial}
                      style={{
                        color: 'white',
                        display: 'inline-block',
                        minWidth: '180px'
                      }}>
                      {`${initial}: `}
                    </label>
                    <input
                      id={initial}
                      onChange={this.handleOnChange}
                    />
                  </div>
                )
              })
            ) : null
          }
        </div>
      </div>
    )
  }
}

export default App
